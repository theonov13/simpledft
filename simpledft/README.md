# simpledft

Main source code directory.

| File           | Description |
| :------------: | :---------: |
| atoms.py       | Atoms class definition |
| dft.py         | DFT routines |
| energies.py    | Energy calculations |
| minimizer.py   | Minimization algorithm |
| operators.py   | Plane wave basis-set dependent operators |
| potentials.py  | Potential |
| scf.py         | SCF class definition |
| utils.py       | Linear algebra and random utilities |
| xc.py          | Exchange-correlation functionals |
